import {createApp} from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import {loadFonts} from './plugins/webfontloader'
import vuetify from "@/plugins/vuetify";
import {firebaseApp} from './plugins/firebase'
import {VueFire, VueFireAuth} from 'vuefire'

loadFonts()

createApp(App)
    .use(router)
    .use(store)
    .use(vuetify)
    .use(VueFire, {
        firebaseApp,
        modules: [
            VueFireAuth()
        ]
    })

    .mount('#app')

