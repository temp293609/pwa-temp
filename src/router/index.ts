import {createRouter, createWebHashHistory, RouteRecordRaw} from 'vue-router'
import HomePage from "@/pages/HomePage.vue";
import AuthPage from "@/pages/AuthPage.vue";

export const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/home',
    name: 'home',
    component: HomePage,
  },
  {
    path: "/auth",
    component: AuthPage,
  },
  {
    path: '/about',
    name: 'about',
    component: () => import(/* webpackChunkName: "about" */ '../pages/AboutView.vue')
  },
  {
    path: '/trip-catalog',
    name: 'trip-catalog',
    component: () => import(/* webpackChunkName: "about" */ '../pages/TripCatalogPage.vue')
  },
  {
    path: '/trip-preview',
    name: 'trip-preview',
    component: () => import(/* webpackChunkName: "about" */ '../pages/TripPreviewPage.vue')
  },
  {
    path: '/trip-creator',
    name: 'trip-creator',
    component: () => import(/* webpackChunkName: "about" */ '../pages/TripCreatorPage.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
