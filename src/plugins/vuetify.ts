// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import 'vuetify/styles'

// Vuetify
import {createVuetify} from 'vuetify'

export default createVuetify(
    {
        theme: {
            themes: {
                light: {
                    colors: {
                        // primary: '#2196F3',
                        // accent: '#0b9de3',
                        // secondary: '#607D8B',
                        // info: '#03A9F4',
                        // success: '#4CAF50',
                        // warning: '#FF9800',
                        // error: '#F44336'

    primary: "#03a9f4",
    secondary: "#4cafff",
    accent: "#607d8b",
    error: "#ff5722",
    warning: "#ffc107",
    info: "#3f51b5",
    success: "#8bc34a"

                    }
                }
            },
        },
        // display: {
        //     mobileBreakpoint: 'xs',
        //     thresholds: {
        //         xs: 400,
        //         sm: 600,
        //         md: 800,
        //         lg: 1000,
        //         xl: 1280,
        //     },
        // },
    }
)
/*
    <VBtn color="primary">Test</VBtn>
    <VBtn color="secondary">Test</VBtn>
    <VBtn color="accent">Test</VBtn>
    <VBtn color="info">Test</VBtn>
    <VBtn color="success">Test</VBtn>
    <VBtn color="warning">Test</VBtn>
    <VBtn color="error">Test</VBtn>
 */
