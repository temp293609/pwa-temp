import {createStore} from 'vuex'
import firebase from "firebase/compat";
import {createUserWithEmailAndPassword, signInWithEmailAndPassword} from "firebase/auth";
import {useFirebaseAuth} from "vuefire";
// import {auth} from '@/plugins/firebase'

export interface State {
    activeRoute: ActiveRoute,
    auth: Auth

}

export interface ActiveRoute {
    id: String,
    displayName: String
}

export interface Auth {
    loggedIn: boolean,
    user?: User
}

export interface User {
    id: string,
    username: string,
    email: string,
}

export default createStore<State>({
    state: {
        activeRoute: {
            id: "trip-catalog",
            displayName: "Trip catalog"
        },
        auth: {
            loggedIn: false
        }
    },
    getters: {
        activeRoute(state) {
            return state.activeRoute.displayName
        }
    },
    mutations: {
        setAuth(state, data) {
            state.auth = {
                loggedIn: true,
                user: {
                    id: data.uid,
                    email: data.email,
                    username: data.displayName,
                }
            };
        }
    },
    actions: {
        async register(context, {email, password}) {
            const auth = useFirebaseAuth()!
            const response = await createUserWithEmailAndPassword(auth, email, password)
        },
        async login(context, {email, password}) {
            const auth = useFirebaseAuth()!
            const response = await signInWithEmailAndPassword(auth, email, password)

            if (response) {
                context.commit("setAuth", response.user)
            }
        }
    },
    modules: {}
})
