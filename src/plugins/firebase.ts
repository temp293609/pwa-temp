import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'
import { enableIndexedDbPersistence } from "firebase/firestore";
import { disableNetwork } from "firebase/firestore";

export const firebaseApp = initializeApp({
    apiKey: "AIzaSyDrzHcPH3AaX6sCVGXqJtB6BTD1u4lG_Fk",
    authDomain: "travel-planner-364318.firebaseapp.com",
    projectId: "travel-planner-364318",
    storageBucket: "travel-planner-364318.appspot.com",
    messagingSenderId: "327726794375",
    appId: "1:327726794375:web:fb306b7b6a6d48a0781a22",
    measurementId: "G-DFXH2GPHNH"
})

const db = getFirestore(firebaseApp)

enableIndexedDbPersistence(db)
    .catch((err) => {
        if (err.code == 'failed-precondition') {
            // Multiple tabs open, persistence can only be enabled
            // in one tab at a a time.
            // ...
        } else if (err.code == 'unimplemented') {
            // The current browser does not support all of the
            // features required to enable persistence
            // ...
        }
    });


// import {getAuth} from 'firebase/auth'
// import firebase from 'firebase/compat/app';
// import 'firebase/compat/firestore';
// import {enableIndexedDbPersistence, initializeFirestore} from "firebase/firestore";
//
// const firebaseConfig = {
//     apiKey: "AIzaSyDrzHcPH3AaX6sCVGXqJtB6BTD1u4lG_Fk",
//     authDomain: "travel-planner-364318.firebaseapp.com",
//     projectId: "travel-planner-364318",
//     storageBucket: "travel-planner-364318.appspot.com",
//     messagingSenderId: "327726794375",
//     appId: "1:327726794375:web:fb306b7b6a6d48a0781a22",
//     measurementId: "G-DFXH2GPHNH"
// };
// const app = firebase.initializeApp(firebaseConfig);
//
// const db = firebase.firestore(app)
// enableIndexedDbPersistence(db)
//     .catch(err => console.error(err));
//
// const auth = getAuth()
//
// export {app, auth}